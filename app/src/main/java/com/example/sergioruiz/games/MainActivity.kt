package com.example.sergioruiz.games

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var txtEmail: EditText? = null
    private var txtPassword: EditText? = null
    private var btnIngresar: Button? = null
    private var txvInfo: TextView? = null
    private var counter: Int = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtEmail = findViewById(R.id.txtEmail)
        txtPassword = findViewById(R.id.txtPassword)
        txvInfo = findViewById(R.id.txvInfo)
        btnIngresar = findViewById(R.id.btnIngresar)

        txvInfo!!.setText("Numero de intentos restantes: 5")

        btnIngresar!!.setOnClickListener {
            validateLogin(txtEmail!!.text.toString(), txtPassword!!.text.toString())
        }
    }

    private fun validateLogin(userEmail: String, userPassword: String) {
        if (userEmail.equals("test@mail.com") && userPassword.equals("test123")) {
            var i = Intent(this@MainActivity, GamesActivity::class.java)
            startActivity(i)
        } else {
            counter.dec()

            txvInfo!!.setText("Numero de intentos restantes: ${counter.toString()}")

            if (counter == 0) {
                btnIngresar!!.isEnabled = false
            }
        }
    }
}
