package com.example.sergioruiz.games.Interfaces

import com.example.sergioruiz.games.Responses.ArrayGames
import io.reactivex.Observable
import retrofit2.http.GET

interface GamesApi {
    @GET("/api/amiibo/?name=mario")
    fun getGamesJson(): Observable<ArrayGames>
}