package com.example.sergioruiz.games.Responses

class ResponseGames {
    var amiiboSeries: String? = null
    var character: String? = null
    var gameSeries: String? = null
    var head: String? = null
    var image: String? = null
    var name: String? = null
    var release: ResponseRelease? = null
    var tail: String? = null
    var type: String? = null
}