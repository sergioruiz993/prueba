package com.example.sergioruiz.games

import android.app.FragmentManager
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.sergioruiz.games.Interfaces.GamesApi
import com.example.sergioruiz.games.Responses.ResponseGames
import com.github.andreilisun.swipedismissdialog.SwipeDismissDialog
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.games_layout.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class GamesActivity: AppCompatActivity() {

    private lateinit var gameAdapter : GameAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.games_layout)

        gameAdapter = GameAdapter()
        games_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        games_list.adapter = gameAdapter

        val retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.baseUrl))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        val apiGames = retrofit.create(GamesApi::class.java)
        apiGames.getGamesJson()
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                gameAdapter.setGames(it.amiibo!!)
            }, {
                Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
            })
    }

    inner class GameAdapter: RecyclerView.Adapter<GameAdapter.GameHolder>(){

        private val gameList: MutableList<ResponseGames> = mutableListOf()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameHolder {
            return GameHolder(layoutInflater.inflate(R.layout.item_game_layout, parent, false))
        }

        override fun getItemCount(): Int {
            return gameList.size
        }

        override fun onBindViewHolder(holder: GameHolder, position: Int) {
            holder.bindModel(gameList[position])
        }

        fun setGames(amiibo: List<ResponseGames>) {
            gameList.addAll(amiibo)
            notifyDataSetChanged()
        }

        inner class GameHolder(itemView: View): RecyclerView.ViewHolder(itemView){
            val txvTitle: TextView = itemView.findViewById(R.id.gameTitle)
            val txvSerie: TextView = itemView.findViewById(R.id.gameSerie)
            val imgAvatarGame: ImageView = itemView.findViewById(R.id.imgGameAvatar)
            val constraintLayout: ConstraintLayout = itemView.findViewById(R.id.constraintClick)

            fun bindModel(game: ResponseGames){
                txvTitle.text = game.name
                txvSerie.text = game.amiiboSeries
                Picasso.with(applicationContext).load(game.image).into(imgAvatarGame)
                constraintLayout.setOnClickListener {
                    var viewDialog: View = LayoutInflater.from(applicationContext).inflate(R.layout.custom_dialog_layout, null)
                    var swipeDismissDialog: SwipeDismissDialog = SwipeDismissDialog.Builder(this@GamesActivity)
                        .setView(viewDialog)
                        .setOnSwipeDismissListener { view, direction ->
                            Toast.makeText(applicationContext, "Cerrado con exito", Toast.LENGTH_SHORT).show()
                        }
                        .build()
                        .show()

                    var txtCharacter = viewDialog.findViewById<TextView>(R.id.txtCharacter)
                    var txtGameSeries = viewDialog.findViewById<TextView>(R.id.txtGameSeries)
                    var txtAu = viewDialog.findViewById<TextView>(R.id.txtAu)
                    var txtEu = viewDialog.findViewById<TextView>(R.id.txtEu)
                    var txtJp = viewDialog.findViewById<TextView>(R.id.txtJp)
                    var txtNa = viewDialog.findViewById<TextView>(R.id.txtNa)
                    var btnOk = viewDialog.findViewById<Button>(R.id.btnOk)

                    txtCharacter.text = game.character
                    txtGameSeries.text = game.gameSeries
                    txtAu.text = "Au: " + game.release!!.au
                    txtEu.text = "Eu: " + game.release!!.eu
                    txtJp.text = "Jp: " + game.release!!.jp
                    txtNa.text = "Na: " + game.release!!.na

                    btnOk.setOnClickListener {
                        swipeDismissDialog.dismiss()
                    }
                }
            }
        }
    }
}