package com.example.sergioruiz.games.Responses

class ResponseRelease {
    var au: String? = null
    var eu: String? = null
    var jp: String? = null
    var na: String? = null
}